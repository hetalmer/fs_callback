/*
    Problem 2:
    
    Using callbacks and the fs module's asynchronous functions, do the following:
        1. Read the given file lipsum.txt
        2. Convert the content to uppercase & write to a new file. Store the name of the new file in filenames.txt
        3. Read the new file and convert it to lower case. Then split the contents into sentences. Then write it to a new file. Store the name of the new file in filenames.txt
        4. Read the new files, sort the content, write it out to a new file. Store the name of the new file in filenames.txt
        5. Read the contents of filenames.txt and delete all the new files that are mentioned in that list simultaneously.
*/
const fs = require("fs");

function fileManagement(filename) {
	let content;
	//read the data from file lipsum.txt
	fs.readFile("../" + filename, function (err, data) {
		if (err) {
			console.error(err);
		}
		// write the data to a file after converting to uppercase to upperContent.txt
		fs.writeFile(
			"../upperContent.txt",
			data.toString().toUpperCase(),
			function (err) {
				if (err) {
					console.error(err);
				}
				// append filename to the filenames.txt
				fs.appendFile("../filenames.txt", "upperContent.txt,", function (err) {
					if (err) {
						console.error(err);
					}
				});
				//read the data from upperContent.txt
				fs.readFile("../upperContent.txt", function (err, data) {
					if (err) {
						console.error(err);
					}
					//convert to lowercase and split it to sentences in each line and write it into lowerContent.txt
					content = data.toString().toLowerCase().split(".");
					let counter = 0;
					content.forEach((element) => {
						counter += 1;
						fs.appendFile(
							"../lowerContent.txt",
							element.trim() + "\n",
							function (err) {
								if (err) {
									console.error(err);
								}
							},
						);
					});
					//append filename to filenames.txt
					if (counter === content.length) {
						fs.appendFile(
							"../filenames.txt",
							"lowerContent.txt,",
							function (err) {
								if (err) {
									console.error(err);
								}
							},
						);
						//read the data from lowerContent.txt
						fs.readFile("../lowerContent.txt", function (err, data) {
							if (err) {
								console.error(err);
							}
							// sort the data and write it into another file sortedContent.txt
							let splitContent = data.toString().split("\n");
							splitContent = splitContent.sort((val1, val2) =>
								val1.localeCompare(val2),
							);
							let splitCounter = 0;
							splitContent.forEach((element) => {
								splitCounter += 1;
								fs.appendFile(
									"../sortedContent.txt",
									element + "\n",
									function (err) {
										if (err) {
											console.error(err);
										}
									},
								);
							});
							//appen filename of sorteddata to filenames.txt
							if (splitCounter === splitContent.length) {
								fs.appendFile(
									"../filenames.txt",
									"sortedContent.txt,",
									function (err) {
										if (err) {
											console.log(err);
										}
										//read the data from filenames.txt
										fs.readFile("../filenames.txt", function (arr, data) {
											if (err) {
												console.error(err);
											}
											//delete all the files listed in filenames.txt
											let deleteFiles = data.toString().split(",");
											deleteFiles.forEach((element) => {
												if (element !== "") {
													fs.unlink("../" + element, function (err) {
														if (err) {
															console.error(err);
														} else {
															console.log(`File ${element} is deleted.`);
														}
													});
												}
											});
										});
									},
								);
							}
						});
					}
				});
			},
		);
	});
}

module.exports = fileManagement;
