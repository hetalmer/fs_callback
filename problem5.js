//In this function I have use different function for different task and call it one by one using callback.
//With using previous code it is very difficult to understand which block containas which block
const fs = require("fs");
const upperFile = "upperFile.txt";
const fileNames = "filenames.txt";
const splitFile = "splitContent.txt";
const sortFile = "sortedContent.txt";
let fileData;
//addFiles function is used to add files in filenames. When data added to file that filename stored with this function
function addFiles(filename) {
	fs.appendFile(fileNames, filename + "\n", function (err) {
		//at the end put '\n' because I want every file name in new line and use it as a newline separator
		if (err) {
			console.error(error);
		}
	});
}
//readData function reads the data from the filename given as argument and it callback the function with data and next function to execute
function readData(filename, callback) {
	fs.readFile(filename, function (err, data) {
		// read the data from file
		if (err) {
			console.error(err);
		} else {
			fileData = data.toString(); //store data in fileData variable
		}
		callback(fileData, splitData); // call the next function to execute
	});
}
//dataToUpper function writes the data to a file after converting to upper case. it also contain callback function which is to be execute next
function dataToUpper(data, callback) {
	fs.writeFile(upperFile, data.toUpperCase(), function (err) {
		// write data to file in uppercase
		if (err) {
			console.error(err);
		}
		addFiles(upperFile); // after writing data store that filename to filenames.txt
		callback(upperFile, sortData); // call back the another function to perform next operation
	});
}
//splitData function reads data from file and split data to sentenses. after that it writes all the data to new file
function splitData(filename, callback) {
	fs.readFile(filename, function (err, data) {
		// read data from previous file
		if (err) {
			console.error(err);
		}
		const splitData = data.toString().toLowerCase().split("."); //split data by .
		splitData.forEach((element) => {
			fs.appendFile(splitFile, element.trim() + "\n", function (err) {
				// append all the data line by line in a file
				if (err) {
					console.error(err);
				}
			});
		});
		addFiles(splitFile); // after writing data store that file name to filename.txt
		callback(splitFile, deleteFiles); //call the function for next operation
	});
}
//sortData function used to sort the data and write it into new file
function sortData(filename, callback) {
	fs.readFile(filename, function (err, data) {
		//read data from file
		if (err) {
			console.error(err);
		}
		const sortData = data
			.toString()
			.split("\n")
			.sort((list1, list2) => list1.localeCompare(list2)); // sort the data by spliting to new line
		sortData.forEach((element) => {
			fs.appendFile(sortFile, element + "\n", function (err) {
				// append that data line by line
				if (err) {
					console.log(err);
				}
			});
		});
		addFiles(sortFile); // adding filename to filenames.txt
		callback(); //call the function for next operation
	});
}
//function to delete all the files from filenames.txt
function deleteFiles() {
	fs.readFile(fileNames, function (err, data) {
		// read the data from file
		if (err) {
			console.error(err);
		}
		files = data.toString().split("\n"); // split it by new line
		files.forEach((element) => {
			if (element !== "") {
				fs.unlink(element, function (err) {
					//delete all the files
					if (err) {
						console.error(err);
					} else {
						console.log(`File ${element} deleted`);
					}
				});
			}
		});
	});
}
readData("lipsum.txt", dataToUpper); // call the function which execute rest of the function
